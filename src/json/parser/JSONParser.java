package json.parser;

import controller.Controller;
import osm.data.OSMNode;
import osm.data.OSMWay;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

    /**
     *
     * @param elements
     * @throws JSONException Elements is part of a JSON file and will be parsed
     * to identify nodes and ways.
     */
    public static void parseElements(List<JSONObject> elements) throws JSONException {
        Controller controller = Controller.getInstance();

        for (JSONObject element : elements) {
            switch (element.getString("type")) {
                case "node":
                    OSMNode newNode = new OSMNode(element);
                    controller.addNode(newNode);
                    break;
                case "way":
                    String highway = "";
                    try {
                        highway = element.getJSONObject("tags").getString("highway");

                    } catch (JSONException e) {
                        highway = null;
                    }
                    if (highway != null && (highway.equals("residential") || highway.equals("motorway") || highway.equals("primary") || highway.equals("secondary") || highway.equals("tertiary")
                            || highway.equals("primary_link") || highway.equals("secondary_link") || highway.equals("tertiary_link") || highway.equals("road") || highway.equals("trunk")
                            || highway.equals("service") || highway.equals("trunk_link") || highway.equals("unclassified"))) {
                        controller.addWay(new OSMWay(element));
                    }
                    break;
            }
        }
    }
}

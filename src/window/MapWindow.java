package window;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.DefaultMapController;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.tilesources.MapQuestOsmTileSource;

public class MapWindow extends JFrame
{
    private static MapWindow mapWindow = null;
    private Controller controller = null;
    private JMapViewer map = null;
    private JPanel panel;
    private final Coordinate COORDINATES_NITEROI = new Coordinate(-22.8992863, -43.0842731);
    private DefaultMapController mapController;
    
    private MapWindow()
    {
        super("OpenStreetMap");
        controller = Controller.getInstance();        
        controller.setMapWindow(this);
        initialize();
        TextWindow.getInstance();
    }
    
    public static MapWindow getInstance()
    {
        if(mapWindow == null)
            mapWindow = new MapWindow();
        return mapWindow;
    }
    
    private void initialize()
    {
        setSize(800, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);        
        setResizable(false);
        map = new JMapViewer();
        map.setTileSource(new MapQuestOsmTileSource());
        map.setDisplayPosition(COORDINATES_NITEROI, 13);
        mapController = new DefaultMapController(map);        
        mapController.setMovementMouseButton(MouseEvent.BUTTON1);
        panel = new JPanel();
        JButton textWindowButton = new JButton("Text popup");
        textWindowButton.setLayout(null);
        textWindowButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {                
                Controller.getInstance().getTextWindow().setVisible(true);
            }
        });
        panel.add(textWindowButton);
        add(panel, BorderLayout.NORTH);                       
        add(map, BorderLayout.CENTER);
        
        setVisible(true);
        
    }
    
    public JMapViewer getMap()
    {
        return this.map;
    }
}

package window;

import algorithms.AStar;
import algorithms.Algorithm;
import algorithms.Annealing;
import algorithms.DijkstraAlgorithm;
import algorithms.NearestNeighbor;
import controller.Controller;
import graph.GraphNode;
import heuristics.Distance;
import heuristics.RoadType;
import osm.data.OSMNode;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.xml.bind.JAXBException;
import org.json.JSONException;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

public class TextWindow extends JFrame implements ActionListener {

    private static TextWindow textWindow = null;
    private Controller controller = null;
    private JPanel textPanel, radioPanel;
    private JTextField startLocationTextField, destinationTextField;
    private ButtonGroup radioGroup;
    private JRadioButton methodA, methodB, methodC, methodD;

    private TextWindow() {
        super("Textbox");
        controller = Controller.getInstance();
        controller.setTextWindow(this);
        initialize();
    }

    public static TextWindow getInstance() {
        if (textWindow == null) {
            textWindow = new TextWindow();
        }
        return textWindow;
    }

    private void initialize() {
        setSize(800, 100);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        setResizable(false);

        textPanel = new JPanel();
        JLabel startLocationLabel = new JLabel("Start: ");
        JLabel destinationLabel = new JLabel("Destination: ");
        startLocationTextField = new JTextField(20);
        destinationTextField = new JTextField(20);
        textPanel.add(startLocationLabel);
        textPanel.add(startLocationTextField);
        textPanel.add(destinationLabel);
        textPanel.add(destinationTextField);

        radioPanel = new JPanel();

        String dijkstra = "Dijkstra algorithm", aStarDistance = "A* with distance heuristic", aStarRoadType = "A* with road type heuristic", simulatedAnnealing = "Simulated annealing";

        methodA = new JRadioButton(dijkstra);
        methodA.setActionCommand(dijkstra);
        methodB = new JRadioButton(aStarDistance);
        methodB.setActionCommand(aStarDistance);
        methodC = new JRadioButton(aStarRoadType);
        methodC.setActionCommand(aStarRoadType);
        methodD = new JRadioButton(simulatedAnnealing);
        methodD.setActionCommand(simulatedAnnealing);

        radioGroup = new ButtonGroup();
        radioGroup.add(methodA);
        radioGroup.add(methodB);
        radioGroup.add(methodC);
        radioGroup.add(methodD);

        radioPanel.add(methodA);
        radioPanel.add(methodB);
        radioPanel.add(methodC);
        radioPanel.add(methodD);

        JButton calculatePathButton = new JButton("Calculate");
        calculatePathButton.addActionListener(this);
        radioPanel.add(calculatePathButton);

        add(radioPanel, BorderLayout.SOUTH);
        add(textPanel, BorderLayout.NORTH);

        setVisible(true);
    }

    public String getStartText() {
        return this.startLocationTextField.getText();
    }

    public String getDestinationText() {
        return this.destinationTextField.getText();
    }

    public String getButtonGroupValue() {
        return this.radioGroup.getSelection().getActionCommand();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        TextWindow textWindow = Controller.getInstance().getTextWindow();
        JMapViewer map = Controller.getInstance().getMapWindow().getMap();
        
        map.removeAllMapMarkers();
        map.removeAllMapPolygons();

        String startLocation = textWindow.getStartText();
        String destination = textWindow.getDestinationText();
        String algorithm = textWindow.getButtonGroupValue();
        
        if(!startLocation.equals("") && !destination.equals("")) {

            List<Coordinate> nominatimCoordinates = findCoords(startLocation, destination);

            try {
                Controller.getInstance().getInfo();
            } catch (IOException ex) {
                Logger.getLogger(TextWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JSONException ex) {
                Logger.getLogger(TextWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
            Algorithm method = null;
            OSMNode nearestNodeToStart = NearestNeighbor.setStartGraphNode(nominatimCoordinates.get(0));
            OSMNode nearestNodeToEnd = NearestNeighbor.setEndGraphNode(nominatimCoordinates.get(1));


            switch (algorithm) {
                case "Dijkstra algorithm":
                    method = new DijkstraAlgorithm(nearestNodeToStart, nearestNodeToEnd);
                    break;
                case "A* with distance heuristic":
                    method = new AStar(nearestNodeToStart, nearestNodeToEnd, new Distance(nearestNodeToEnd));
                    break;
                case "A* with road type heuristic":
                    method = new AStar(nearestNodeToStart, nearestNodeToEnd, new RoadType());
                    break;
                case "Simulated annealing":
                    method = new Annealing(nearestNodeToStart, nearestNodeToEnd);
                    break;
            }
            if(method != null) {
                List<GraphNode> pathNodes = method.search(method.getInitialNodes(), method.getfinalNodes());

                List<Coordinate> route = new ArrayList();

                for (GraphNode node : pathNodes) {
                    OSMNode initialNode = node.getStartNode();
                    Coordinate initialCoordinate = new Coordinate(initialNode.getCoords().getLat(), initialNode.getCoords().getLon());
                    OSMNode finalNode = node.getEndNode();
                    Coordinate finalCoordinate = new Coordinate(finalNode.getCoords().getLat(), finalNode.getCoords().getLon());

                    route.add(initialCoordinate);
                    route.add(finalCoordinate);
                    route.add(finalCoordinate);

                    map.addMapPolygon(new MapPolygonImpl(route));
                    route = new ArrayList();
                }
            }
        }
    }

    private List<Coordinate> findCoords(String startLocation, String destination) {
        Coordinate startLocationCoordinates = null, destinationCoordinates = null;
        try {
            startLocationCoordinates = Controller.getInstance().getCoordinates(startLocation);
            destinationCoordinates = Controller.getInstance().getCoordinates(destination);
        } catch (JAXBException ex) {
            Logger.getLogger(TextWindow.class.getName()).log(Level.SEVERE, null, ex);
        }

        JMapViewer map = Controller.getInstance().getMapWindow().getMap();

        map.addMapMarker(new MapMarkerDot(startLocationCoordinates));
        map.addMapMarker(new MapMarkerDot(destinationCoordinates));
        map.setDisplayToFitMapMarkers();
        map.setDisplayToFitMapPolygons();
        List<Coordinate> coordinates = new ArrayList<>(Arrays.asList(startLocationCoordinates, destinationCoordinates));

        return coordinates;
    }
}

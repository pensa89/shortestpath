package heuristics;

import algorithms.HaversineFormula;
import osm.data.OSMNode;

public class Distance extends Heuristic {
    
    private OSMNode destinationNode;
    
    public Distance(OSMNode destinationNode) {
        super();
        this.destinationNode = destinationNode;
    }
    
    public OSMNode getDestinationNode() {
        return this.destinationNode;
    }

    @Override
    public double calculateHeuristic() {
        return HaversineFormula.haversine(currentNode.getCoords().getLat(), currentNode.getCoords().getLon(), destinationNode.getCoords().getLat(), destinationNode.getCoords().getLon());
    }
    
}

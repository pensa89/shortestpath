package heuristics;

import osm.data.OSMNode;

public abstract class Heuristic {    
    
    protected OSMNode currentNode;
    
    public Heuristic() {
        this.currentNode = null;
    }
    
    public OSMNode getCurrentNode() {
        return this.currentNode;
    }
    
    public void setCurrentNode(OSMNode node) {
        this.currentNode = node;
    }
    
    public abstract double calculateHeuristic();
}

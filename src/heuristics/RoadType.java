package heuristics;

import controller.Controller;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import osm.data.OSMWay;

public class RoadType extends Heuristic {
    
    private Map<String, Integer> roadType;
        
    public RoadType() {
        super();
        this.roadType = new HashMap();
        populateMap();
    }
    
    private void populateMap() {
        this.roadType.put("motorway", 10);
        this.roadType.put("residential", 5);
        this.roadType.put("trunk", 9);
        this.roadType.put("primary", 8);
        this.roadType.put("secondary", 7);
        this.roadType.put("tertiary", 6);
        this.roadType.put("service", 2);
        this.roadType.put("motorway_link", 10);
        this.roadType.put("trunk_link", 9);
        this.roadType.put("primary_link", 8);
        this.roadType.put("secondary_link", 7);
        this.roadType.put("tertiary_link", 6);
        this.roadType.put("unclassified", 2);
        this.roadType.put("", 1);
    }

    @Override
    public double calculateHeuristic() {
        String highway = "";
        for (OSMWay way : Controller.getInstance().getOSMWays().getWayList()) {
            if(way.getWayId() == currentNode.getWayId()) {
                try {
                    highway = way.getWay().getJSONObject("tags").getString("highway");
                } catch (JSONException ex) {
                    highway = "";                    
                }                
                break;
            }
        }
        return this.roadType.get(highway);
    }
    
}

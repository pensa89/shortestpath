package osm.data;

import org.json.JSONException;
import org.json.JSONObject;
import org.openstreetmap.gui.jmapviewer.Coordinate;

/* Model for an OSM node */
public class OSMNode {

    private int id;
    private int wayId;
    private Coordinate coordinates;
    private JSONObject node;

    public OSMNode(JSONObject obj) throws JSONException {
        this.id = obj.getInt("id");
        Double latitude = obj.getDouble("lat");
        Double longitude = obj.getDouble("lon");
        this.coordinates = new Coordinate(latitude, longitude);
        this.node = obj;
        this.wayId = -1;
    }

    public Coordinate getCoords() {
        return this.coordinates;
    }

    public int getNodeId() {
        return this.id;
    }

    public JSONObject getNode() {
        return this.node;
    }

    public int getWayId() {
        return this.wayId;
    }

    public void setWayId(int id) {
        this.wayId = id;
    }

    @Override
    public boolean equals(Object o) {
        OSMNode other = (OSMNode) o;
        return this.getNodeId() == other.getNodeId();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        return hash;
    }
}

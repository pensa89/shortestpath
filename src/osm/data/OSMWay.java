package osm.data;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* Model for OSM way */
public class OSMWay 
{
    private int id;
    private List<Integer> nodeIds = new ArrayList<>();
    private JSONObject way;
    
    public OSMWay(JSONObject obj) throws JSONException
    {
        this.id = obj.getInt("id");
        JSONArray nodeIdsArray = obj.getJSONArray("nodes");
        for (int i = 0; i < nodeIdsArray.length(); i++) 
        {
            this.nodeIds.add(nodeIdsArray.getInt(i));
        }
        this.way = obj;
    }
    
    public int getWayId()
    {
        return this.id;
    }
    
    public List<Integer> getNodeIdList()
    {
        return this.nodeIds;
    }
    
    public JSONObject getWay()
    {
        return this.way;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.id;
        return hash;
    }
    
    @Override
    public boolean equals(Object o)
    {
        OSMWay other = (OSMWay) o;
        return this.getWayId() == other.getWayId();
    }
                    
}

package algorithms;

import controller.Controller;
import graph.GraphNode;
import heuristics.Heuristic;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import osm.data.OSMNode;

public class AStar extends Algorithm {

    private PriorityQueue<AStarNode> frontier;
    private Set<GraphNode> explored;
    private List<GraphNode> neighbors;
    private GraphNode nodeGraph;
    private AStarNode node, childNode;
    private double pathCost, cost;
    private final Heuristic heuristic;

    public AStar(OSMNode initialNode, OSMNode endNode, Heuristic h) {
        super(initialNode, endNode);
        this.frontier = new PriorityQueue(11, new AStarComparator());
        this.explored = new HashSet();
        this.heuristic = h;
    }

    @Override
    public List<GraphNode> search(Set<GraphNode> startNodes, Set<GraphNode> endNodes) {

        //Creates initial state. Finds the best start node if there's more than one.
        cost = 0;
        pathCost = Integer.MAX_VALUE;
        for (GraphNode initial : startNodes) {
            cost = HaversineFormula.haversine(initial.getStartNode().getCoords().getLat(), initial.getStartNode().getCoords().getLon(), initial.getEndNode().getCoords().getLat(), initial.getEndNode().getCoords().getLon());
            if (pathCost > cost) {
                pathCost = cost;
                nodeGraph = initial;
            }
        }

        //Adds the tree root.
        frontier.add(new AStarNode(null, nodeGraph, heuristic));

        do {
            if (frontier.isEmpty()) {
                return new ArrayList();
            }

            node = frontier.poll();

            if (endNodes.contains(node.getState())) {
                return solution(node);
            }

            explored.add(node.getState());
            neighbors = Controller.getInstance().getGraph().getGraphEdges().getEdgesNeighborhoodMap().get(node.getState());

            for (GraphNode cur : neighbors) {
                childNode = new AStarNode(node, cur, heuristic);
                AStarNode old = null;

                for (AStarNode f : frontier) {
                    if (f.getState().equals(childNode.getState())) {
                        old = f;
                        break;
                    }
                }

                if (!explored.contains(childNode.getState()) && (old == null)) {
                    frontier.add(childNode);
                    
                } else if ((old != null) && (old.getEvaluation() > childNode.getEvaluation())) {
                    old.setParent(childNode.getParent());

                }
            }

        } while (!frontier.isEmpty());

        return new ArrayList<>();
    }

    private List<GraphNode> solution(AStarNode node) {
        List<GraphNode> sol = new ArrayList();
        AStarNode cur = node;

        sol.add(cur.getState());
        while (cur.getParent() != null) {
            cur = cur.getParent();
            sol.add(cur.getState());
        }
        sol.add(cur.getState());

        return sol;
    }
}

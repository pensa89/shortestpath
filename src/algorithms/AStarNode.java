package algorithms;

import graph.GraphNode;
import heuristics.Heuristic;

public class AStarNode {

    private AStarNode parent;
    private GraphNode state;
    private Heuristic heuristic;
    private double pathCost, heuristicValue, evaluation;

    public AStarNode(AStarNode parent, GraphNode state, Heuristic h) {
        this.parent = parent;
        this.state = state;
        this.heuristic = h;
        
        heuristic.setCurrentNode(this.state.getStartNode());
        this.heuristicValue = heuristic.calculateHeuristic();
        
        calculateEvaluation();
    }

    private void calculateEvaluation(){
        
        if (this.parent != null) {
            this.pathCost = parent.getPathCost();
        } else {
            this.pathCost = 0;
        }
        
        this.pathCost += HaversineFormula.haversine(state.getStartNode().getCoords().getLat(), state.getStartNode().getCoords().getLon(), state.getEndNode().getCoords().getLat(), state.getEndNode().getCoords().getLon());
        this.evaluation = this.pathCost + this.heuristicValue;
    }
    
    public GraphNode getState() {
        return this.state;
    }

    public AStarNode getParent() {
        return this.parent;
    }

    public double getPathCost() {
        return this.pathCost;
    }
    
    public double getEvaluation(){
        return this.evaluation;
    }
    
    public void setParent(AStarNode parent){
        this.parent = parent;
        calculateEvaluation();
    }
}

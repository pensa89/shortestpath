package algorithms;

import java.util.Comparator;

public class AStarComparator implements Comparator{

    @Override
    public int compare(Object o1, Object o2) {
        return (int) Math.signum(((AStarNode) o1).getEvaluation() - ((AStarNode) o2).getEvaluation());
    }
}

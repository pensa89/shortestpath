package algorithms;

import controller.Controller;
import data.structures.GraphNodes;
import graph.GraphNode;
import osm.data.OSMNode;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class NearestNeighbor
{
    /**
     * 
     * @param startCoords
     * @return The nearest node to start location
     */
    public static OSMNode setStartGraphNode(Coordinate startCoords)
    {       
        GraphNodes graphNodes = Controller.getInstance().getGraph().getGraphNodes();
        OSMNode startNode = null;
        double minimalDistance = Double.MAX_VALUE;
        for(GraphNode node : graphNodes.getGraphNodesSet())
        {
            OSMNode temp = node.getStartNode();
            double latitude = temp.getCoords().getLat();
            double longitude = temp.getCoords().getLon();
            double distance = HaversineFormula.haversine(latitude, longitude, startCoords.getLat(), startCoords.getLon());
            if(distance < minimalDistance)
            {
                minimalDistance = distance;
                startNode = temp;
            }
        }
        return startNode;
    }
    
    /**
     * 
     * @param startCoords
     * @return The nearest node to destination
     */
    public static OSMNode setEndGraphNode(Coordinate startCoords)
    {
        GraphNodes graphNodes = Controller.getInstance().getGraph().getGraphNodes();
        OSMNode endNode = null;
        double minimalDistance = Double.MAX_VALUE;
        for(GraphNode node : graphNodes.getGraphNodesSet())
        {
            OSMNode temp = node.getEndNode();
            double latitude = temp.getCoords().getLat();
            double longitude = temp.getCoords().getLon();
            double distance = HaversineFormula.haversine(latitude, longitude, startCoords.getLat(), startCoords.getLon());
            if(distance < minimalDistance)
            {
                minimalDistance = distance;
                endNode = temp;
            }
        }
        return endNode;
    }
}

package algorithms;

import controller.Controller;
import graph.GraphNode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import osm.data.OSMNode;

public class Annealing extends Algorithm {

    public Annealing(OSMNode initialNode, OSMNode endNode) {
        super(initialNode, endNode);
    }

    @Override
    public List<GraphNode> search(Set<GraphNode> startNodes, Set<GraphNode> endNodes) {
        double step = 1000;
        int iterations = 250000;
        GraphNode currentNode = new ArrayList<>(startNodes).get(new Random().nextInt(startNodes.size()));
        List<GraphNode> nodes = new ArrayList();
        nodes.add(currentNode);

        while (currentNode.getEndNode() != endNode && iterations > 0) {
            List<GraphNode> neighborhood;
            GraphNode randomNeighbor = null;

            neighborhood = Controller.getInstance().getGraph().getGraphEdges().getEdgesNeighborhoodMap().get(currentNode);           
            
            if (!neighborhood.isEmpty()) {
                randomNeighbor = neighborhood.get(new Random().nextInt(neighborhood.size()));
            } else {         
                do {
                    currentNode = nodes.get(new Random().nextInt(nodes.size()));
                    neighborhood = Controller.getInstance().getGraph().getGraphEdges().getEdgesNeighborhoodMap().get(currentNode);
                    if(!neighborhood.isEmpty())
                        randomNeighbor = neighborhood.get(new Random().nextInt(neighborhood.size()));
                } while(neighborhood.isEmpty());
            }

            double neighborEval = HaversineFormula.haversine(randomNeighbor.getStartNode().getCoords().getLat(), randomNeighbor.getStartNode().getCoords().getLon(), endNode.getCoords().getLat(), endNode.getCoords().getLon());
            double currentNodeEval = HaversineFormula.haversine(currentNode.getStartNode().getCoords().getLat(), currentNode.getStartNode().getCoords().getLon(), endNode.getCoords().getLat(), endNode.getCoords().getLon());

            double deltaE = neighborEval - currentNodeEval;
            if (deltaE < 0 || new Random().nextDouble() < Math.exp(deltaE / step)) {
                currentNode = randomNeighbor;
                nodes.add(randomNeighbor);
            }
            step *= 0.80;
            iterations--;
        }
        return nodes;
    }

}

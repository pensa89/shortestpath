package algorithms;

import graph.GraphNode;
import osm.data.OSMNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class DijkstraAlgorithm extends Algorithm {

    private final List<OSMNode> nodes;
    private final List<GraphNode> edges;
    private Set<OSMNode> settledNodes;
    private Set<OSMNode> unSettledNodes;
    private Map<OSMNode, OSMNode> predecessors;
    private Map<OSMNode, Double> distance;

    public DijkstraAlgorithm(OSMNode start, OSMNode end) {

        super(start, end);
        this.nodes = new ArrayList<OSMNode>(controller.Controller.getInstance().getOSMNodes().getNodeList());
        this.edges = new ArrayList<GraphNode>(controller.Controller.getInstance().getGraph().getGraphNodes().getGraphNodesSet());

    }

    public void execute(OSMNode source) {

        settledNodes = new HashSet<OSMNode>();
        unSettledNodes = new HashSet<OSMNode>();
        distance = new HashMap<OSMNode, Double>();
        predecessors = new HashMap<OSMNode, OSMNode>();

        distance.put(source, 0.0);
        unSettledNodes.add(source);

        while (unSettledNodes.size() > 0) {
            OSMNode node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(OSMNode node) {

        List<OSMNode> adjacentNodes = getNeighbors(node);
        double shortestDistanceToTarget, shortestDistanceToNode, distanceBetween;

        for (OSMNode target : adjacentNodes) {
            shortestDistanceToTarget = getShortestDistance(target);
            shortestDistanceToNode = getShortestDistance(node);
            distanceBetween = getDistance(node, target);
            if (shortestDistanceToTarget > shortestDistanceToNode + distanceBetween) {
                distance.put(target, getShortestDistance(node) + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    private double getDistance(OSMNode node, OSMNode target) {

        for (GraphNode edge : edges) {
            if (edge.getStartNode().equals(node) && edge.getEndNode().equals(target)) {
                return edge.getWeight();
            }
        }

        throw new RuntimeException("Algo deu errado.");
    }

    private List<OSMNode> getNeighbors(OSMNode node) {

        List<OSMNode> neighbors = new ArrayList<OSMNode>();
        Coordinate initialCoordinate = node.getCoords();

        for (GraphNode edge : edges) {
            if (edge.getStartNode().getCoords() == initialCoordinate && !isSettled(edge.getEndNode())) {
                neighbors.add(edge.getEndNode());
            }
        }

        return neighbors;
    }

    private OSMNode getMinimum(Set<OSMNode> nodes) {

        OSMNode minimum = null;

        for (OSMNode node : nodes) {
            if (minimum == null) {
                minimum = node;
            } else {
                if (getShortestDistance(node) < getShortestDistance(minimum)) {
                    minimum = node;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(OSMNode node) {
        return settledNodes.contains(node);
    }

    private double getShortestDistance(OSMNode destination) {

        Double d = distance.get(destination);

        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    public LinkedList<OSMNode> getPath(OSMNode target) {

        LinkedList<OSMNode> path = new LinkedList<OSMNode>();
        OSMNode step = target;

        if (predecessors.get(step) == null) {
            return new LinkedList<OSMNode>();
        }

        path.add(step);

        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }

        Collections.reverse(path);
        return path;
    }

    @Override
    public List<GraphNode> search(Set<GraphNode> startNodes, Set<GraphNode> endNodes) {
        this.execute(initialNode);
        LinkedList<OSMNode> path = this.getPath(endNode);
        List<GraphNode> shortestPath = new ArrayList<GraphNode>();
        GraphNode graphNode;

        for (int i = 0; i < path.size() - 1; i++) {
            graphNode = new GraphNode(path.get(i), path.get(i + 1));
            shortestPath.add(graphNode);
        }

        return shortestPath;
    }

}

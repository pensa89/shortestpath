package algorithms;

import controller.Controller;
import graph.GraphNode;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import osm.data.OSMNode;

public abstract class Algorithm {

    protected Set<GraphNode> initialNodes, finalNodes;
    protected OSMNode initialNode, endNode;

    public Algorithm(OSMNode initialNode, OSMNode endNode) {
        this.initialNode = initialNode;
        this.endNode = endNode;
        initialNodes = findInitialGraphNodes(initialNode);
        finalNodes = findFinalGraphNodes(endNode);
    }

    public Set<GraphNode> getInitialNodes() {
        return this.initialNodes;
    }

    public Set<GraphNode> getfinalNodes() {
        return this.finalNodes;
    }

    public OSMNode getInitialNode() {
        return this.initialNode;
    }

    public OSMNode getEndNode() {
        return this.endNode;
    }
    
    private Set<GraphNode> findInitialGraphNodes(OSMNode startNode) {
        Set<GraphNode> nodes = new HashSet();
        Set<GraphNode> graphNodeSet = Controller.getInstance().getGraph().getGraphNodes().getGraphNodesSet();
        for (GraphNode node : graphNodeSet) {
            if (node.getStartNode() == startNode) {
                nodes.add(node);
            }
        }
        return nodes;
    }

    private Set<GraphNode> findFinalGraphNodes(OSMNode endNode) {
        Set<GraphNode> nodes = new HashSet();
        Set<GraphNode> graphNodeSet = Controller.getInstance().getGraph().getGraphNodes().getGraphNodesSet();
        for (GraphNode node : graphNodeSet) {
            if (node.getEndNode() == endNode) {
                nodes.add(node);
            }
        }
        return nodes;
    }
    
    public abstract List<GraphNode> search(Set<GraphNode> startNodes, Set<GraphNode> endNodes);
}

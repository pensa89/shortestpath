package controller;

import data.structures.OSMNodes;
import data.structures.OSMWays;
import graph.Graph;
import graph.GraphBuilder;
import osm.data.OSMNode;
import osm.data.OSMWay;
import json.parser.JSONParser;
import nominatim.Nominatim;
import overpass.Overpass;
import window.MapWindow;
import window.TextWindow;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openstreetmap.gui.jmapviewer.Coordinate;

/* Part of the MVC pattern */
public class Controller 
{
    private static Controller controller = null;
    private TextWindow textWindow;
    private MapWindow mapWindow;
    
    private OSMNodes nodes;
    private OSMWays ways;
    private Graph graph;
    private boolean firstTime = true;
        
    private Controller() 
    {
        this.nodes = new OSMNodes();
        this.ways = new OSMWays();
        this.graph = Graph.getInstance();
    }
    
    public static Controller getInstance()
    {
        if(controller == null)
            controller = new Controller();
        return controller;
    }
    
    public TextWindow getTextWindow()
    {
        return this.textWindow;
    }
    
    public MapWindow getMapWindow()
    {
        return this.mapWindow;
    }
        
    public void setTextWindow(TextWindow window)
    {
        this.textWindow = window;
    }
    
    public void setMapWindow(MapWindow window)
    {
        this.mapWindow = window;
    }
    
    public Coordinate getCoordinates(String location) throws JAXBException
    {
        return Nominatim.getCoordinates(location.trim());
    }
    
    public void getInfo() throws IOException, MalformedURLException, JSONException
    {
        if(firstTime) {
            firstTime = false;
            JSONArray elements = Overpass.getInfoJSON();

            List<JSONObject> elementsList = new ArrayList<>();
            for (int i = 0; i < elements.length(); i++) 
            {
                elementsList.add(elements.getJSONObject(i));
            }

            JSONParser.parseElements(elementsList);
            GraphBuilder builder = new GraphBuilder();
            builder.buildGraph();
        }
    }
    
    public void addNode(OSMNode n)
    {
        this.nodes.getNodeList().add(n);
        this.nodes.getNodeMap().put(n.getNodeId(), n);
    }
    
    public void addWay(OSMWay w)
    {
        this.ways.getWayList().add(w);
        this.ways.getWayMap().put(w.getWayId(), w.getNodeIdList());
    }
    
    public Graph getGraph()
    {
        return this.graph;
    }
    
    public OSMNodes getOSMNodes()
    {
        return this.nodes;
    }
    
    public OSMWays getOSMWays()
    {
        return this.ways;
    }
    
    public void setOSMWays(OSMWays ways)
    {
        this.ways = ways;
    }
    
    public void setOSMNodes(OSMNodes nodes)
    {
        this.nodes = nodes;
    }
    
    public void setGraph(Graph g)
    {
        this.graph = g;
    }
}

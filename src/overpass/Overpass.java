package overpass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Overpass {

    public static JSONArray getInfoJSON() throws IOException, JSONException {
        JSONObject jsonObj = null;
        JSONArray elements = null;

        //Most ofNiterói
        /*double lat1 = -22.9954;
        double long1 = -43.1804;
        double lat2 = -22.8531;
        double long2 = -42.9109;*/
        
        //Icaraí, Centro, São Francisco, Charitas, Jurujuba, Ingá, São Domingos, Fonseca, Boa Viagem
        double lat1 = -22.9405, long1 = -43.1400, lat2 = -22.8670, long2 = -43.0736;
        
        URL url = new URL("http://overpass.osm.rambler.ru/cgi/interpreter?data=[out:json];(node(" + lat1 + "," + long1 + "," + lat2 + "," + long2 + ");rel(bn)->.x;way(" + lat1 + "," + long1 + "," + lat2 + "," + long2 + ");node(w)->.x;rel(bw););out%20meta;");
        InputStream is = url.openStream();
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            StringBuilder jsonText = new StringBuilder();
            String line = "";
            while ((line = rd.readLine()) != null) {
                jsonText.append(line);
            }
            jsonObj = new JSONObject(jsonText.toString());
            elements = jsonObj.getJSONArray("elements");

        } finally {
            is.close();
            if(rd != null)
                rd.close();
        }
        return elements;
    }
}

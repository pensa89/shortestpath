package nominatim;

import java.io.IOException;
import java.net.URL;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Nominatim 
{
    /**
     * 
     * @param location
     * @return Latitude and longitude of location
     * @throws JAXBException 
     * Method to aquire the latitude and longitude of a location from a XML.
     */
    public static Coordinate getCoordinates(String location) throws JAXBException 
    {
        double latitude = 0, longitude = 0;
        location = StringParsing.replaceSpaces(location);
        
        try 
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();                      
            
            Document map = db.parse(new URL("http://nominatim.openstreetmap.org/search/" + location + "?format=xml&addressdetails=1&limit=1&polygon_svg=1").openStream(), "UTF-8");
            
            NodeList placeList = map.getElementsByTagName("place");
                                    
            latitude = Double.parseDouble(placeList.item(0).getAttributes().getNamedItem("lat").getNodeValue());
            longitude = Double.parseDouble(placeList.item(0).getAttributes().getNamedItem("lon").getNodeValue());

        } catch (IOException e) {
            System.out.println(e);
        } catch (SAXException e) {
            System.out.println(e);
        } catch (ParserConfigurationException e) {
            System.out.println(e);
        }
        
        return new Coordinate(latitude, longitude);
    }
}

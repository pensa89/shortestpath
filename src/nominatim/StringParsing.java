package nominatim;

public class StringParsing 
{
    /**
     * 
     * @param text
     * @return text without spaces.
     * Replace all white spaces by %20.
     */
    public static String replaceSpaces(String text)
    {
        return text.replaceAll(" ", "%20");
    }
}

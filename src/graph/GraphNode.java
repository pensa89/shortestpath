package graph;

import osm.data.OSMNode;
import java.util.Objects;
import algorithms.HaversineFormula;

/* Model for a graph node */
public class GraphNode {

    private OSMNode start, end;
    private double weight;

    public GraphNode(OSMNode start, OSMNode end) {
        this.start = start;
        this.end = end;
        this.weight = HaversineFormula.haversine(start.getCoords().getLat(), start.getCoords().getLon(), end.getCoords().getLat(), end.getCoords().getLon());

    }

    public OSMNode getStartNode() {
        return this.start;
    }

    public OSMNode getEndNode() {
        return this.end;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        GraphNode other = (GraphNode) o;
        return this.getStartNode().getNodeId() == other.getStartNode().getNodeId() && this.getEndNode().getNodeId() == other.getEndNode().getNodeId();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.start);
        hash = 97 * hash + Objects.hashCode(this.end);
        return hash;
    }
}

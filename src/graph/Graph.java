package graph;

import data.structures.GraphEdges;
import data.structures.GraphNodes;

/* Class to store graph nodes and edges */
public class Graph 
{
    private static Graph graph = null;
    private GraphEdges graphEdges;
    private GraphNodes graphNodes;
    
    private Graph()
    {
        this.graphEdges = GraphEdges.getInstance();
        this.graphNodes = GraphNodes.getInstance();
    }
    
    public static Graph getInstance()
    {
        if(graph == null)
            graph = new Graph();
        return graph;
    }
    
    public GraphEdges getGraphEdges()
    {
        return this.graphEdges;
    }
    
    public GraphNodes getGraphNodes()
    {
        return this.graphNodes;
    }
    
    public void setGraphNodes(GraphNodes ns)
    {
        this.graphNodes = ns;
    }
    
    public void setGraphEdges(GraphEdges es)
    {
        this.graphEdges = es;
    }
}

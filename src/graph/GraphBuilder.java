package graph;

import controller.Controller;
import data.structures.GraphEdges;
import data.structures.GraphNodes;
import data.structures.OSMNodes;
import data.structures.OSMWays;
import osm.data.OSMNode;
import osm.data.OSMWay;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;

public class GraphBuilder {

    private Graph graph = null;
    private GraphEdges graphEdges = null;
    private GraphNodes graphNodes = null;
    private OSMNodes osmNodes = null;
    private OSMWays osmWays = null;

    public GraphBuilder() {
        this.graph = Controller.getInstance().getGraph();

        this.graphEdges = graph.getGraphEdges();
        this.graphNodes = graph.getGraphNodes();

        this.osmNodes = Controller.getInstance().getOSMNodes();
        this.osmWays = Controller.getInstance().getOSMWays();
    }

    /**
     * Method responsible for creating graph nodes and graph edges from raw OSM
     * data.
     */
    public void buildGraph() {
        for (OSMWay way : this.osmWays.getWayList()) {
            createGraphNodes(way);
        }
        createGraphNodesNeighborhood();
    }

    private void createGraphNodes(OSMWay way) {
        createNodesFromWayOrder(way);
        String oneWay = "";
        try {
            oneWay = way.getWay().getJSONObject("tags").getString("oneway");
        } catch (JSONException ex) {
            oneWay = null;
        }
        if(oneWay != null && oneWay.equals("no"))
            createNodesFromReverseWayOrder(way);
    }

    private void createNodesFromWayOrder(OSMWay way) {
        for (int i = 0; i < way.getNodeIdList().size() - 1; i++) {
            int startNodeId = way.getNodeIdList().get(i);
            int endNodeId = way.getNodeIdList().get(i + 1);

            OSMNode startNode = osmNodes.getNodeMap().get(startNodeId);
            OSMNode endNode = osmNodes.getNodeMap().get(endNodeId);

            // Check for oneway and eliminate u-turns?
            if (startNode != null && endNode != null) {
                startNode.setWayId(way.getWayId());
                endNode.setWayId(way.getWayId());
                graphNodes.getGraphNodesSet().add(new GraphNode(startNode, endNode));
            }
        }
    }

    private void createNodesFromReverseWayOrder(OSMWay way) {
        for (int i = way.getNodeIdList().size() - 1; i < 0; i--) {
            int startNodeId = way.getNodeIdList().get(i);
            int endNodeId = way.getNodeIdList().get(i - 1);

            OSMNode startNode = osmNodes.getNodeMap().get(startNodeId);
            OSMNode endNode = osmNodes.getNodeMap().get(endNodeId);

            // Check for oneway and eliminate u-turns?
            if (startNode != null && endNode != null) {
                startNode.setWayId(way.getWayId());
                endNode.setWayId(way.getWayId());
                graphNodes.getGraphNodesSet().add(new GraphNode(startNode, endNode));
            }
        }
    }

    private void createGraphNodesNeighborhood() {
        for (GraphNode graphNode1 : this.graphNodes.getGraphNodesSet()) {
            List<GraphNode> neighborhood = new ArrayList();
            for (GraphNode graphNode2 : this.graphNodes.getGraphNodesSet()) {
                if (graphNode1.getEndNode() == graphNode2.getStartNode() && graphNode1.getStartNode() != graphNode2.getEndNode()) {
                    neighborhood.add(graphNode2);
                }
            }
            this.graphEdges.getEdgesNeighborhoodMap().put(graphNode1, neighborhood);
        }
    }
}

package graph;

import java.util.Objects;

/* Model for a graph edge */
public class GraphEdge 
{
    private GraphNode fromNode, toNode;
    
    public GraphEdge(GraphNode from, GraphNode to)
    {
        this.fromNode = from;
        this.toNode = to;
    }
    
    public GraphNode getFromNode()
    {
        return this.fromNode;
    }            
    
    public GraphNode getToNode()
    {
        return this.toNode;
    }
    
    @Override
    public boolean equals(Object o)
    {
        GraphEdge other = (GraphEdge) o;
        return this.getFromNode() == other.getFromNode() && this.getToNode() == other.getToNode();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.fromNode);
        hash = 61 * hash + Objects.hashCode(this.toNode);
        return hash;
    }
}

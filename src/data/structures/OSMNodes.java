package data.structures;

import osm.data.OSMNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* Class to store OSM nodes and information related to it */
public class OSMNodes 
{
    /* List of OSM nodes */
    private List<OSMNode> nodeList = new ArrayList();
    /* Map with key as node id and value as the coordinates of the node */
    private Map<Integer, OSMNode> nodeMap = new HashMap();
        
    public List<OSMNode> getNodeList()
    {
        return this.nodeList;
    }
    
    public Map<Integer, OSMNode> getNodeMap()
    {
        return this.nodeMap;
    }
}

package data.structures;

import osm.data.OSMWay;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* Class to store OSM ways and information related to it */
public class OSMWays 
{
    /* List os OSM ways */
    private List<OSMWay> wayList = new ArrayList();
    /* The map key is the way id and the value is the list of node ids */
    private Map<Integer, List<Integer>> wayMap = new HashMap();
    
    public List<OSMWay> getWayList()
    {
        return this.wayList;
    }
    
    public Map<Integer, List<Integer>> getWayMap()
    {
        return this.wayMap;
    }
}

package data.structures;

import graph.GraphEdge;
import graph.GraphNode;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* Class to store graph edges and information related to it */
public class GraphEdges 
{
    private static GraphEdges graphEdges = null;
    private Set<GraphEdge> graphEdgesSet = new HashSet();
    private Map<GraphNode, List<GraphNode>> edgeEndPointMap = new HashMap();
    
    private GraphEdges() { }
    
    public static GraphEdges getInstance()
    {
        if(graphEdges == null)
            graphEdges = new GraphEdges();
        return graphEdges;
    }
    
    public Set<GraphEdge> getGraphEgdesSet()
    {
        return this.graphEdgesSet;
    }
    
    public Map<GraphNode, List<GraphNode>> getEdgesNeighborhoodMap()
    {
        return this.edgeEndPointMap;
    }
}

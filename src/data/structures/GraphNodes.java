package data.structures;

import graph.GraphNode;
import java.util.HashSet;
import java.util.Set;

/* Class to store graph nodes and information related to it */
public class GraphNodes 
{
    private static GraphNodes graphNodes = null;
    private Set<GraphNode> graphNodesSet = new HashSet();
    
    private GraphNodes() { }
    
    public static GraphNodes getInstance()
    {
        if(graphNodes == null)
            graphNodes = new GraphNodes();
        return graphNodes;
    }
    
    public Set<GraphNode> getGraphNodesSet()
    {
        return this.graphNodesSet;
    }
}
